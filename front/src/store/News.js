import newsApi from '../../api/news'

export default {
    state: {
        news: []
    },
    getters: {
        allNews(state) {
            return state.news
        }
    },
    action: {
        getNews({commit}) {
            newsApi.getNewsList()
                .then((response) => {
                    commit('updateNews', response.data)
                })
        }
    },
    mutations: {
        updateNews(state, news) {
            state.news = news
        }
    }
}