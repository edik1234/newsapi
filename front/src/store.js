import Vue from 'vue'
import Vuex from 'vuex'
import News from './store/News'
import Catalog from './store/Catalog'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        News,
        Catalog
    }
})
