import Vue from 'vue'
import Router from 'vue-router'
const Home = () => import('./views/Home.vue')
const Contacts = () => import('./views/Contacts.vue')
const About = () => import('./views/About')
const PageNotFound = () => import('./views/PageNotFound')
const CatalogIdex = () => import('./views/CatalogSectionList')
const Stock = () => import('./views/Stock')
const CatalogListItem = () => import('./components/CatalogListItem')
const CatalogList = () => import('./components/CatalogList')

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: Contacts
        },
        {
            path: '/catalog',
            name: 'catalog',
            component: CatalogIdex,
            children: [
                {
                    path: '/:name',
                    name: 'countryname',
                    component: CatalogList,
                    children: [
                        {
                            path: '/:id',
                            name: 'idtour',
                            component: CatalogListItem
                        }
                    ]
                }
            ]
        },
        {
            path: '/stock',
            name: 'stock',
            component: Stock
        },
        {
            path: '*',
            name: 'PageNotFound',
            component: PageNotFound
        }
    ]
})
