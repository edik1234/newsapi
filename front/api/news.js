import axios from 'axios'

export default {
    getNewsList: function () {
        return axios.get('news/getnews')
    }
}

