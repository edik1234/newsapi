var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(cors())
const port = 8081;

require('./route/routes')(app);

const urlencodedParser = bodyParser.urlencoded({extended: false})

app.listen(port, function (err) {
  if(err) {
    return console.log('Read problem: ', err)
  }
  console.log('app listening on port ',port)
});