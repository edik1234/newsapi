var tools = require('../tools/news')

exports.getnew = function () {
    let sql = "SELECT * FROM news"
    return tools.get(sql)
}
exports.addnew = function (params) {
    let post = {name: `${params.name}`, discription: `${params.discription}`};
    let sql = 'INSERT INTO news SET ?'
    return tools.post(sql, post)
}
