module.exports = function (app) {
    const newsRouter = require('./newsRouter')
    const catalog = require('./catalog')

    app.use('/ta', newsRouter, catalog)
}