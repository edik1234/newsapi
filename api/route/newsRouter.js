var express = require('express')
var router = express.Router()

const controller = require('../controller/news')
// const assign = require('../tools/tools')

// var URLS = [
//     {
//         method: 'get',
//         path: '/getnews',
//         handler: 'controller.getnews'
//     },
//     {
//         method: 'post',
//         path: '/addnews',
//         handler: 'controller.addnews'
//     }
// ]
// URLS.forEach(() => {
//     router.assign(URLS.method, URLS.path, URLS.handler)
// })

router.post('/addnews', controller.addnews)
router.get('/getnews', controller.getnews)

module.exports = router