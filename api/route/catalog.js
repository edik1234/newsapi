var express = require('express')
var router = express.Router()

const controller = require('../controller/catalog')

router.get('/catalog', controller.getCatalog)
router.get('/catalog/city')

module.exports = router