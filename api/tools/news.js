const db = require('../config/dbconnect')

exports.get = function (sql) { 
    return new Promise((resolve, reject) => {
        db.query(sql, (error, results) => {
            if (error) {
                reject(error)
            }
            resolve(results)
        })
    })
}

exports.post = function (sql, post) {
    return new Promise((resolve,reject) => {
        db.query(sql, post, (error, results) => {
            if (error) {
                reject(error)
            }
            resolve(results)
        })
    })
}