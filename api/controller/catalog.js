const catalog = require('../models/catalog')

exports.getCatalog = (req, res) => {
    return new Promise((resolve) => {
        catalog.getCat().then((results) => {
            res.send(results)
            resolve(results)
        })
    })
        .catch((e) => {
            console.error('Have problem1: ', e)
        })
}