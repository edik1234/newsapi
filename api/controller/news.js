const news = require('../models/news')

//GET ALL NEWS
exports.getnews = (req, res) => {
    return new Promise((resolve) => {
        news.getnew().then((results) => {
            res.send(results)
            resolve(results)
        })
    })
        .catch((e) => {
            console.error('Have problem1: ', e)
        })
}

//ADD new news
exports.addnews = (req, res) => {
    return new Promise((resolve) => {
        if (req.body.name == '' || req.body.discription == '') {
            console.log('Write something')
        }
        else {
            news.addnew(req.body).then((results) => {
                console.log('You add 1 post')
                resolve(results)
            })
        }
    })
        .catch((e) => {
            console.error('Have problem1: ', e)
        })
}